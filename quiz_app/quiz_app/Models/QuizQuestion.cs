﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace quiz_app.Models
{
    public class QuizQuestion
    {   
        [Key]
        public int QuestionId { get; set; }
        public string Question { get; set; }
        public string choice1 { get; set; }
        public string choice2 { get; set; }
    }
}
