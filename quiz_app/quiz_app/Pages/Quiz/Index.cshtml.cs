using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using quiz_app.Data;
using quiz_app.Models;

namespace quiz_app.Pages.Quiz
{
    public class IndexModel : PageModel
    {

        private readonly ApplicationDbContext _db;

        public IndexModel(ApplicationDbContext db)
        {
            _db = db;
        }
        public List<QuizQuestion> Questions { get; set; }

        public Dictionary<int, string> Answers { get; set; } 

        public bool isDone { get; set; }
        public async Task OnGetAsync()
        {
            Questions = await _db.QuizQuestions.ToListAsync(); 
        }

        public void OnPost()
        {
            isDone = true;
        }
    }
}
