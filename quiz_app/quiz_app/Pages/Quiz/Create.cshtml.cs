using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using quiz_app.Data;
using quiz_app.Models;

namespace quiz_app.Pages.Quiz
{
    public class CreateModel : PageModel
    {
        private readonly ApplicationDbContext _db;

        public CreateModel(ApplicationDbContext db)
        {
            _db = db;
        }

        [BindProperty]

        public QuizQuestion Question { get; set; }
        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPost()
        {
            await _db.QuizQuestions.AddAsync(Question);
            await _db.SaveChangesAsync();
            return RedirectToPage("Index");
        }
    }
}
